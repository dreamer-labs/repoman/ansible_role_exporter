ansible_role_exporter
=========

Role for exporting and packaging roles from git repos. If you are using docker images you can configure to pull down docker images and package for importing. 

Using Role
----------
This role is designed to be run as a molecule test. It will create a container that will pull down all of the repos/images and create an archive of them. Then the are saved to the local filesystem with a datetime stamp. 

```
molecule test
```
Currently all of the archives are saved in the same directory as the molecule test scenario. 

```
ansible_role_exporter/molecule/default/<< datetime >>-exported.image.<< format >>
```

Requirements
------------

This role is setup to use molecule to run the tests and perform the exports. Need all requires to be able to run ansible and molecule.

Role Variables
--------------

All of the role variables are defined in the defaults/main.

Repository export variables
  - exporter_format:
    - Defaults to gz, can be changed to bz2, tar, xz, or zip
  - exporter_designation:
    - Location used for exporting repos, this is relative to the container that is created.
  - exporter_archive_name:
    - Defaults to exported-repos.{{ exporter_format }}
    - This shouldn't be changed, the molecule test looks specifically when saving the file out of the container. 
  - exporter_repos:
    - List of all git repos to download.

Docker Image export variables
  - exporter_docker_image_export:
    - Boolean to disable to enable docker image exports
  - exporter_docker_image_destination:
    - Location used for exporting docker images, this is relative to the container is that created.
  - export_docker_images_archive_name: 
    - Defaults to exported-image.{{ exporter_format }}
    - This shouldn't be changed, the molecule test looks specifically when saving the file out of the container. 

exporter_repos:
  - List of all git repos to download.

License
-------

MIT

Author Information
------------------
The Development Range Engineering, Architecture, and Modernization (DREAM) Team
